package main;

import ExchangeClasses.Message;
import Mongo.MongoUtils;

import java.net.MalformedURLException;

public class main {
    public static void main(String[] args) {
        try {
            Message[] msgs = null;

            for (int i = 0; i < 100; i++) {
                Long bevore = System.currentTimeMillis();
                msgs = MongoUtils.getAllMessages();
                System.out.println("Packet: "+i+", time: "+(System.currentTimeMillis()-bevore)/10e3+"s");
            }
            for (Message msg :
                    msgs) {
                System.out.println(msg.getSender()+" "+msg.getText());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}

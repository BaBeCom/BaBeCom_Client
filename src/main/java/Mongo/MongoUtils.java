package Mongo;

import ExchangeClasses.Message;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class MongoUtils {
    public static Message[] getAllMessages() throws MalformedURLException {
        URL url = new URL("http://23.100.8.22:6666/ws/mongows?wsdl");

        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://Mongo/", "MongoWSImplService");
        Service service = Service.create(url, qname);
        MongoWS hello = service.getPort(MongoWS.class);
        return hello.getAllMessages();
    }
}
